#!/bin/bash 
. ./log_config.sh
time_hash=`date +%F---%H-%M`
dir="/tmp/$time_hash"
b='1'

while [[ -n "$1" ]]; do
  param=$1
  shift
  case $param in
      --log_types)
      log_types=$1
      shift
      ;;
      --days)
      days=$1
      shift
      ;;
      --debug)
      debug=$1
      shift
      ;;
  esac
done


if [[ 1 == $debug ]]
then
set -x
fi

logs=(`echo $log_types`)

mkdir -p $dir

     while IFS=',' read -ra ITEMS; do       #  парсинг параметров конфига
     for item in "${ITEMS[@]}"; do          #
         filename=${!item}                  #
         if [[ ! -z "${filename}" ]]; then  #

              if [[ 0 == $days ]]           
              then
                  cp "${filename}" $dir
              else              
                  cp "${filename}" $dir
                  for (( c=1; c <= $days; c++ ))          
                     do 
                      if [[ -f "${filename}".$c.gz ]]; 
                        then
                        cp "${filename}".$c.gz $dir
                        else
                        echo "no logs fo this day "${filename}".$c.gz" >> $dir/readme  
                        fi
                        done
                        fi
           else   #
           echo "wrong input parameters" #
           exit -1 #
         fi#
     done #
done <<< "${logs}" #



tar -cf $dir/$time_hash.${hostname}log.tar $dir/* 2> /dev/null
bzip2 -c < $dir/$time_hash.${hostname}log.tar > $dir/$time_hash.${hostname}log.tar.bz2
rm $dir/$time_hash.${hostname}log.tar
cp $dir/$time_hash.${hostname}log.tar.bz2 $path_to_public
echo "$portal_url$time_hash.${hostname}log.tar.bz2"
rm -rf $dir

