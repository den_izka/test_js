# вот это прописано в файле-конфиге
gui_access_log=/var/log/www/streamline/access.log
gui_error_log=/var/log/www/streamline/error.log

# это сам скрипт который парсит
logs="gui_access_log,gui_error_log"
while IFS=',' read -ra ITEMS; do
     for item in "${ITEMS[@]}"; do
         filename=${!item}
         if [[ ! -z "${filename}" ]]; then
           echo "${filename}"
         else
           echo "wrong input parameters"
           exit -1
         fi
     done
done <<< "${logs}"
